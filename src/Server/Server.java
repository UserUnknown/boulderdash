package Server;

import game.Gamer;
import game.GamerEvent;
import game.RandomGenerator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server implements Gamer {
	private GamerEvent event;
	private Map<String, ClientCover> clients = new ConcurrentHashMap<>();
	Boolean started = false;
	private Boolean looped = false;
	private ServerSocket server;
	private Thread serverThread;
	private int playerID = 0;
	private int sequence = 1;
	public DataOutputStream output;
	private String mapName;
	private int cordinateX;
	private int cordinateY;
	RandomGenerator randPlace = new RandomGenerator();
	public boolean isrun() {

		return this.started;
	}

	@Override
	public int getCordinateX() {
	
		this.cordinateX = randPlace.a;
		
		return this.cordinateX;
	}

	@Override
	public int getCordinateY() {
		this.cordinateY = randPlace.b;
		return this.cordinateY;
	}

	@Override
	public int getPlayerID() {

		return this.playerID;
	}

	@Override
	public void setEvent(GamerEvent event) {
		this.event = event;
	}

	private String readString(DataInputStream stream) throws IOException {
		int dataLength = stream.readInt();
		byte[] data = new byte[dataLength];

		while (stream.available() < dataLength) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		stream.read(data); // TODO: upewnic sie czy nie trzeba czekac na dane

		return new String(data);
	}

	public Server(String mapName) {

		this.mapName = mapName;
	}

	public boolean run(int port) {
		// TODO: tworzenie serwera uruchomienie servera .ok
		if (started)
			return false;

		serverThread = new Thread() {
			@Override
			public void run() {

				try {
					server = new ServerSocket(port);

					event.playerConnected(playerID, "login", mapName,getCordinateX(), getCordinateY());

					while (looped) // w watku nasluchujemy klientow
					{
						ClientCover cover = new ClientCover();
						cover.socket = server.accept();

						cover.input = new DataInputStream(
								cover.socket.getInputStream());
						cover.output = new DataOutputStream(
								cover.socket.getOutputStream());

						String login = readString(cover.input);
						cover.randplaceX = cover.input.readInt();
						cover.randplaceY = cover.input.readInt();
						Boolean correct = true;

						synchronized (clients) {
							if (clients.containsKey(login)) {
								correct = false;
							}

							clients.put(login, cover);
						}

						if (correct) {
							cover.output.writeInt(sequence);
							cover.output.writeUTF(mapName);
							cover.playerID = sequence;
							sequence++;

							try {
								synchronized (cover) {
									cover.output.writeByte(3);
									cover.output.writeInt(cover.playerID); //wysyla zly playerID chyba
									byte[] data = "serverlogin".getBytes();
									cover.output.writeInt(data.length);
									cover.output.write(data);
									// wysy�anie informacji o nazwie mapy
									byte[] mapData = mapName.getBytes();
									cover.output.writeInt(mapData.length);
									cover.output.write(mapData);
									// wysyla inf o koordynatach gracza
									cover.output.writeInt(cordinateX);
									cover.output.writeInt(cordinateY);

								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							for (ClientCover el : clients.values()) {
								if (el.playerID != cover.playerID) {
									try {
										synchronized (el) {
											el.output.writeByte(3);
											el.output.writeInt(cover.playerID);
											byte[] data = login.getBytes();
											el.output.writeInt(data.length);
											el.output.write(data);
											// wys�anie infomacji o nazwie mapy
											// do pozosta�ych client�w
											byte[] mapData = mapName.getBytes();
											el.output.writeInt(mapData.length);
											el.output.write(mapData);
											cover.output.writeInt(cordinateX);
											cover.output.writeInt(cordinateY);
										}
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}

						Server.this.event.playerConnected(cover.playerID,login, mapName, cover.randplaceX,cover.randplaceY);
								//po  co to???
							while (looped) // w watku kolejmyn zaakceptowanego
											// klienta
							{
								switch (cover.input.readByte()) {
								case 1:
									int move = cover.input.readByte();

									event.moveReceived(move, cover.playerID);

									for (ClientCover el : clients.values()) {
										try {
											synchronized (el) {
												el.output.writeByte(1);
												el.output.writeByte((byte) move);
												el.output.writeInt(cover.playerID);
											}
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}

									break;

								case 2:

									String message = readString(cover.input);
									event.messageReceived(message);

									byte[] data = message.getBytes();

									for (ClientCover el : clients.values()) {
										try {
											synchronized (el) {
												el.output.writeByte(2);
												el.output.writeInt(data.length);
												el.output.write(data);
											}
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}

									break;
								}
							}
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		looped = true;
		serverThread.setName("Watek serwera");
		serverThread.start();
		started = true;
		return true;
	}

	public void stop() {
		if (started) {
			looped = false;
			try {
				server.close();
				// .input.close(); nie dzia�a
				// ClientCover.output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				serverThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // /funkcja czekacjaca az zakonczy sie watek servera po przerwaniu
				// p�tli WAZNE

			started = false;

		}
	}

	@Override
	public void move(int move) {

		this.event.moveReceived(move, playerID);

		for (ClientCover el : this.clients.values()) {
			try {
				synchronized (el) {
					el.output.writeByte(1);
					el.output.writeByte((byte) move);
					el.output.writeInt(playerID);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void sendMessage(String message) {

		byte[] data = message.getBytes();

		for (ClientCover el : this.clients.values()) {
			try {
				synchronized (el) {
					el.output.writeByte(2);
					el.output.writeInt(data.length);
					el.output.write(data);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class ClientCover {
		public Socket socket;
		public DataInputStream input;
		public DataOutputStream output;
		public int playerID;
		public int randplaceX;
		public int randplaceY;
	}

	@Override
	public String getMapName() {

		return mapName;
	}

}
