package game;

public interface GamerEvent {
	
	public void playerConnected(int playerID,String login,String gameName,int randPlaceX,int randPlaceY);
	
	public void moveReceived(int move, int playerID);

	public void messageReceived(String message);

}
