package game;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import menu.DataBase;
import menu.Hscore;
import menu.LoginEvent;
import menu.Menu;
import menu.Multiplayer;
import menu.NewGame;
import menu.Rules;
import menu.Menu.HandlerClass;

public class InsertLogin extends JFrame {

	public static LoginEvent event;
	public String login = null;
	public String password = null;
	JTextField userText = new JTextField(20);
	JPasswordField passwordText = new JPasswordField(20);
	JLabel infoText = new JLabel("Sing up/Log in or play as a guest");
	JButton registerButton = new JButton("register");
	JButton loginButton = new JButton("login");
	JButton noRejestrationButton = new JButton("Guest Game");
	DataBase dataBase;

	public void setLoginEvent(LoginEvent event)

	{
		this.event = event;
	}

	public InsertLogin() {

		super("Login");
		JPanel panel = new JPanel();
		this.add(panel);

		this.setVisible(true);

		userText.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					login = userText.getText();
					event.loginRecived(login);
				} else
					login = "default";

			}
		});

		panel.setLayout(null);

		JLabel userLabel = new JLabel("User");
		userLabel.setBounds(10, 10, 80, 25);
		panel.add(userLabel);

		userText.setBounds(100, 10, 160, 25);
		panel.add(userText);

		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 40, 80, 25);
		panel.add(passwordLabel);

		infoText.setBounds(30, 160, 239, 25);
		panel.add(infoText);

		passwordText.setBounds(100, 40, 160, 25);
		panel.add(passwordText);

		loginButton.setBounds(10, 80, 80, 25);
		panel.add(loginButton);

		noRejestrationButton.setBounds(90, 120, 120, 25);
		panel.add(noRejestrationButton);

		registerButton.setBounds(200, 80, 80, 25);
		panel.add(registerButton);

		HandlerClass handler = new HandlerClass();

		loginButton.addActionListener(handler);
		registerButton.addActionListener(handler);
		noRejestrationButton.addActionListener(handler);

	}

	private class HandlerClass implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			Object source = event.getSource();

			if (source == loginButton) {
				login = userText.getText();
				password = passwordText.getText();

				if (userText.getText().hashCode() == 0
						|| passwordText.getText().hashCode() == 0)
					infoText.setText("Your login/password cannot be empty!");

				else
					dataBase = new DataBase(login, password, 2);

			} else if (source == registerButton) {

				login = userText.getText();
				password = passwordText.getText();

				if (userText.getText().hashCode() == 0
						|| passwordText.getText().hashCode() == 0)
					infoText.setText("Your login/password cannot be empty!");

				else
					dataBase = new DataBase(login, password, 1);	
		
			}

			else if (source == noRejestrationButton) {

				login = userText.getText();
				if ("".equals(userText.getText()))// userText.getText().hashCode() == 0)
					login = "Guest";
				InsertLogin.this.event.loginRecived(login);

			}
		}
	}
	
	
}
