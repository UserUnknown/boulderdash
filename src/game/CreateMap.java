package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import Client.Client;
import GameObject.Background;
import GameObject.Diamond;
import GameObject.Grass;
import GameObject.Playerobj;
import GameObject.Stone;
import Server.Server;
import game.GameObjects;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class CreateMap extends JPanel {

	private Image dbImage;
	private Graphics dbGraphics;
	private List<GameObjects> objects = new ArrayList<GameObjects>();
	int[][] arr = new int[34][25];
	double time = 0.00;
	int[] diamondcounter = new int[5];
	public static Boolean gameover = false;
	public String msg;
	private List<Stone> stoneList = new ArrayList<Stone>();
	private Map<Integer, Player> playerList = new ConcurrentHashMap<Integer, Player>();
	Boolean stap = false;
	public String login = "default";
	Gamer gamer;
	private String mapName;
	public static final int MOVE_UP = 0;
	public static final int MOVE_DOWN = 1;
	public static final int MOVE_LEFT = 2;
	public static final int MOVE_RIGHT = 3;
	public Boolean multi;
	Point playerposition;

	public CreateMap(String gamename, Boolean multi, Boolean newPlayer,
			String login) {

		this.multi = multi;
		this.mapName = gamename;

		if (multi) {
			if (newPlayer == false) {

				Server server = new Server(gamename);
				server.run(5000);
				gamer = server;
			} else {
				Client client = new Client();
				client.connect("127.0.0.1", 5000, login);
				gamer = client;
			}

			gamer.setEvent(new GamerEvent() {
				@Override
				public void moveReceived(int move, int playerID) {

					switch (move) {

					case Gamer.MOVE_UP:
						playerMove(playerID, MOVE_UP);
						break;
					case Gamer.MOVE_DOWN:
						playerMove(playerID, MOVE_DOWN);
						break;

					case Gamer.MOVE_LEFT:
						playerMove(playerID, MOVE_LEFT);
						break;

					case Gamer.MOVE_RIGHT:
						playerMove(playerID, MOVE_RIGHT);
						break;
					}

					repaint();

				}

				@Override
				public void messageReceived(String message) {

					GameScreen.chatWindow.append(message);

				}

				@Override
				public void playerConnected(int playerID, String login,
						String gameName, int randPlaceX, int randPlaceY) {

					addPlayer(playerID, login, randPlaceX, randPlaceY);
				}
			});

		} else
			addPlayer(0, login, 5, 5);

		if (multi)
			mapName = gamer.getMapName();

		this.setFocusable(true);

		objects.add(new Grass());
		objects.add(new Stone(-1, -1));
		objects.add(new Diamond());
		objects.add(new Background());
		objects.add(new Playerobj());

		for (int i = 0; i < 5; i++)
			diamondcounter[i] = 0;
		try {
			String line;
			BufferedReader br = new BufferedReader(new FileReader(mapName));
			int j = 0;
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < line.length(); i++) {
					arr[j][i] = Integer.parseInt(line.substring(i, 1 + i));
					if (arr[j][i] == 2)
						stoneList.add(new Stone(j, i));
				}
				j++;
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Nie znaleziono pliku");
		} catch (IOException e) {
		}

		timeCounter(true);
		this.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {

				case KeyEvent.VK_LEFT:

					if (multi)
						gamer.move(Gamer.MOVE_LEFT);

					else
						playerMove(0, MOVE_LEFT);
					break;
				case KeyEvent.VK_RIGHT:

					if (multi)
						gamer.move(Gamer.MOVE_RIGHT);
					else
						playerMove(0, MOVE_RIGHT);
					break;
				case KeyEvent.VK_UP:

					if (multi)

						gamer.move(Gamer.MOVE_UP);
					else
						playerMove(0, MOVE_UP);
					break;
				case KeyEvent.VK_DOWN:

					if (multi)
						gamer.move(Gamer.MOVE_DOWN);
					else
						playerMove(0, MOVE_DOWN);
					break;

				case KeyEvent.VK_ENTER:

					final SendMessage chatMassage = new SendMessage();
					chatMassage.setMessageEvent(new MessageEvent() {

						@Override
						public void MessegeRecived(String msg) {

							if (newPlayer == false)
								GameScreen.chatWindow.append(msg + "\n");

							gamer.sendMessage(msg + "\n");
							chatMassage.dispose();
						}
					});
					chatMassage.setSize(300, 55);
					chatMassage.setVisible(true);
					chatMassage.setResizable(false);
					chatMassage.setLocationRelativeTo(null);

					System.out.println(msg);

					break;
				}

				repaint();

				// try { Thread.sleep(100); } catch(InterruptedException ex) {}

			}
		});
	}

	@Override
	public void paint(Graphics g) {
		// super.paint(g);

		if (dbGraphics == null) {
			dbImage = createImage(990, 720);
			dbGraphics = dbImage.getGraphics();
		}

		paintComponent(dbGraphics);
		g.drawImage(dbImage, 0, 0, this);
	}

	public void paintComponent(Graphics g) {

		Boolean wingame = true;

		for (int i = 0; i < 33; i++) {
			for (int j = 0; j < 23; j++) {
				if (arr[i][j] == 1)
					g.drawImage(((Grass) (objects.get(0))).objpic, (i * 30),
							(j * 30), 30, 30, null);

				else if (arr[i][j] == 2)
					g.drawImage(((Stone) (objects.get(1))).objpic, (i * 30),
							(j * 30), 30, 30, null);

				else if (arr[i][j] == 3) {
					g.drawImage(((Diamond) (objects.get(2))).objpic, (i * 30),
							(j * 30), 30, 30, null);
					wingame = false;
				} else if (arr[i][j] == -1)
					g.drawImage(((Background) (objects.get(3))).objpic,
							(i * 30), (j * 30), 30, 30, null);
			}
		}

		for (int i = 0; i < stoneList.size(); i++) {

			if (arr[stoneList.get(i).x][stoneList.get(i).y + 1] == -1)
				rollingStones(stoneList.get(i).x, stoneList.get(i).y, i);

		}

		g.setFont(new Font("MV Boli", Font.ITALIC, 20));
		g.setColor(Color.RED);
		if (multi)
			g.drawString("Diamonds: " + diamondcounter[gamer.getPlayerID()],
					850, 30);
		else
			g.drawString("Diamonds: " + diamondcounter[0], 850, 30);
		g.drawString(String.format("Time: %.2f", time), 850, 50);
		if (wingame == true) {
			stap = true;
			winGameMessage();
		}
		for (Player player : playerList.values()) {

			Point playerpositionall = player.returnPosition();

			g.drawImage(((Playerobj) (objects.get(4))).objpic,
					playerpositionall.x, playerpositionall.y, 30, 30, null);
		}
	}

	void checkObject() {
		if (multi)
			playerposition = playerList.get(gamer.getPlayerID())
					.returnPosition();
		else
			playerposition = playerList.get(0).returnPosition();

		playerposition.x /= 30;
		playerposition.y /= 30;
		if (arr[playerposition.x][playerposition.y] == 1) {
			arr[playerposition.x][playerposition.y] = -1; // zmien z trawy na
															// t�o!
			try {
				AudioInputStream audio = AudioSystem
						.getAudioInputStream(new File("lib/jar/trawasound.wav")); // wczytanie
																					// d�wi�k�w
				Clip clip = AudioSystem.getClip();
				clip.open(audio);
				clip.start();
			} catch (UnsupportedAudioFileException uae) {
				System.out.println(uae);
			} catch (IOException ioe) {
				System.out.println(ioe);
			} catch (LineUnavailableException lua) {
				System.out.println(lua);
			}
		} else if (arr[playerposition.x][playerposition.y] == 3) {
			arr[playerposition.x][playerposition.y] = -1;
			if (multi)
				diamondincrease(gamer.getPlayerID());
			else
				diamondincrease(0);
			try {
				AudioInputStream audio = AudioSystem
						.getAudioInputStream(new File(
								"lib/jar/diamentsound.wav"));
				Clip clip = AudioSystem.getClip();
				clip.open(audio);
				clip.start();
			} catch (UnsupportedAudioFileException uae) {
				System.out.println(uae);
			} catch (IOException ioe) {
				System.out.println(ioe);
			} catch (LineUnavailableException lua) {
				System.out.println(lua);
			}
		}
	}

	int diamondincrease(int playerID) {

		return this.diamondcounter[playerID]++;
	}

	void winGameMessage() {
		gameover = true;
		WinGameMessage w = new WinGameMessage(time);
		w.setSize(300, 100);
		w.setVisible(true);
		w.setResizable(false);
		w.setLocationRelativeTo(null);
	}

	void timeCounter(Boolean start) // Funkcja licz�ca czas
	{
		Thread t = new Thread() {
			@Override
			public void run() {
				while (start) {
					try {
						Thread.sleep(10);
						time = time + 0.01;
						repaint();
						break;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		};
		t.start();
	}

	void rollingStones(int x, int y, int i) // Funkcja dotycz�ca upadku kamieni
	{
		boolean isrun = false;

		Thread t1 = new Thread() {
			@Override
			public void run() {
				while (arr[x][y + 1] == -1 && !gameover) {
					try {
						Thread.sleep(500);
						arr[x][y] = -1;
						stoneList.get(i).y = y + 1;
						arr[x][y + 1] = 2;
						if (multi)
							playerposition = playerList
									.get(gamer.getPlayerID()).returnPosition();
						else
							playerposition = playerList.get(0).returnPosition();
						playerposition.x /= 30;
						playerposition.y /= 30;
						repaint();
						if (arr[playerposition.x][playerposition.y] == 2) {
							gameover = true;
							gameOver();
						}

					} catch (InterruptedException e) {
					}
					;
				}
			}
		};
		t1.start();

	}

	void gameOver() {
		timeCounter(false);
		JOptionPane.showMessageDialog(null, "Player " + login + " lose!");
		System.exit(0);
	}

	void playerMove(int id, int direction) {
		Player player = playerList.get(id);

		if (player == null)
			return;

		Point playerposition = player.returnPosition();
		if (direction == MOVE_UP) {
			if (arr[playerposition.x / 30][playerposition.y / 30 - 1] != 2)
				player.moveVertical(-1);
			if (playerposition.y <= 1)
				player.moveVertical(1);
			checkObject();
		}

		else if (direction == MOVE_DOWN) {
			if (arr[playerposition.x / 30][playerposition.y / 30 + 1] != 2)
				player.moveVertical(1);
			if (playerposition.y >= 690)
				player.moveVertical(-1);
			checkObject();
		} else if (direction == MOVE_LEFT) {
			if (arr[playerposition.x / 30 - 1][playerposition.y / 30] != 2)
				player.moveHorizontal(-1);
			if (playerposition.x <= 0)
				player.moveHorizontal(1);
			checkObject();
		} else if (direction == MOVE_RIGHT) {
			if (arr[playerposition.x / 30 + 1][playerposition.y / 30] != 2)
				player.moveHorizontal(1);
			if (playerposition.x >= 960)
				player.moveHorizontal(-1);
			checkObject();
		}
	}

	void addPlayer(int playerID, String login, int randPlaceX, int randPlaceY) {

		if (!playerList.containsKey(playerID)) {
			playerList.put(playerID, new Player(randPlaceX, randPlaceY,
					playerID));
		} else
			System.out.println("powtorzenie playera");
	}

}
