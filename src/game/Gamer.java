package game;

public interface Gamer {

	public static final int MOVE_UP = 0; 
	public static final int MOVE_DOWN = 1;
	public static final int MOVE_LEFT = 2;
	public static final int MOVE_RIGHT = 3;

	public int getCordinateX();
	public int getCordinateY();
	
	public int getPlayerID();
	
	public void setEvent(GamerEvent event);

	public void move(int move);   // czy drugim argumentem nie powinno by� playerID?

	public void sendMessage(String message);
	public String getMapName();
}
