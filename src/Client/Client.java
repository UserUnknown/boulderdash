package Client;

import game.Gamer;
import game.GamerEvent;
import game.RandomGenerator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Gamer {
	private GamerEvent event;
	private Socket socket;
	private DataInputStream input;
	private DataOutputStream output;
	Boolean started = false;
	private Boolean looped = false;
	private Thread clientThread;
	public int playerID11;
	private int cordinateX;
	private int cordinateY;
	String mapName;
	RandomGenerator randPlace= new RandomGenerator();
	public boolean isConnected() {
		return this.started;
	}
	
	@Override
	public int getCordinateX() {
		
		this.cordinateX=randPlace.a;
		return this.cordinateX;
	}

	@Override
	public int getCordinateY() {
		
		this.cordinateY=randPlace.b;
		return this.cordinateY;
	}
	private String readString(DataInputStream stream) throws IOException {
		int dataLength = stream.readInt();
		byte[] data = new byte[dataLength];

		while (stream.available() < dataLength) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		stream.read(data); // TODO: upewnic sie czy nie trzeba czekac na dane

		return new String(data);
	}
	@Override
	public int getPlayerID() {
		
		return this.playerID11;
	}
	@Override
	public void setEvent(GamerEvent event) {
		this.event = event;
	}

	public boolean connect(String address, int port, String login) {

		if (started)
			return false;
		
		try {
		socket = new Socket(address, port);

		input = new DataInputStream(socket.getInputStream());
		output = new DataOutputStream(socket.getOutputStream());

		// TODO: wyslanie loginu .ok
		getCordinateX();
		getCordinateY();
		byte[] loginData = login.getBytes();

		output.writeInt(loginData.length);
		output.write(loginData);
		output.writeInt(getCordinateX());
		output.writeInt(getCordinateY());
		Client.this.playerID11=input.readInt();
		mapName=input.readUTF();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			
			//TODO: pozamykaŠ strumienie,sockety
			
			return false;
			
		}
		clientThread = new Thread() {
			@Override
			public void run() {
				try {

					// /przypisac uzytkownikowi login

					while (looped) // w osobnym watku petla
					{
						switch (input.readByte()) {
						case 1: // ramka typu ruch
							int move = input.readByte();
							int playerID=input.readInt();
							event.moveReceived(move, playerID);

							break;

						case 2: // ramka typu wiadomosc
							
							String message=readString(input);
							event.messageReceived(message);

							break;
							
						case 3://ramka typu klient connected
							int playerID2= input.readInt();
							String login=readString(input);
							String gameMap=readString(input);
							int randPlaceX=input.readInt();
							int randPlaceY=input.readInt();
							event.playerConnected(playerID2,login,gameMap,randPlaceX,randPlaceY);
							//tutaj dodaje serwer do listy
							event.playerConnected(2, "player2", mapName, cordinateX, cordinateY);
							//a tutaj playera
							break;
						}
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};

		looped = true;
		clientThread.setName("Watek clienta");
		clientThread.start();
		started = true;
		return true;
	}

	public void disconnect() {

		if (started) {
			looped = false;
			try {
				this.socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				clientThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			started = false;
		}
	}

	@Override
	public void move(int move) {
		try {
			this.output.writeByte(1);
			this.output.writeByte((byte) move);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(String message) {

		byte[] data = message.getBytes();

		try {
			this.output.writeByte(2);
			this.output.writeInt(data.length);
			this.output.write(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getMapName() {

		return mapName;
	}

	

}
