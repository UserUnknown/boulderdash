import java.awt.event.KeyListener;

import game.InsertLogin;
import game.SendMessage;

import javax.swing.JFrame;

import menu.LoginEvent;
import menu.Menu;



public class Main {
	

	public static void main(String[] args){


		final InsertLogin insertLogin = new InsertLogin();
		
		insertLogin.setLoginEvent(new LoginEvent() 
		{

			@Override
			public void loginRecived(String login) {
			
				System.out.println(login);
				Menu menu = new Menu(login);
				menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				menu.setSize(500,550);
				menu.setVisible(true);
				menu.setLocationRelativeTo(null);
				insertLogin.dispose();
			}

			@Override
			public void noPlayer() {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		insertLogin.setSize(300, 220);
		insertLogin.setVisible(true);
		insertLogin.setResizable(false);
		insertLogin.setLocationRelativeTo(null);
		
	
		
		
		
		
		
	}
}
